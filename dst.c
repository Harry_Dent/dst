#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

// this value was found through experiment
int threshold = 8192;

int FastSN(double*, double*, double*, double*, int, int);
int FastTN(double*, double*, double*, double*, int, int);
int FastUN(double*, double*, double*, double*, int, int);
int FastSN_parallel(double*, double*, double*, double*, int, int);
int FastUN_parallel(double*, double*, double*, double*, int, int);
int FastTN_parallel(double*, double*, double*, double*, int, int);

int FastSN_serial(double*, double*, double*, double*, int, int);
int FastTN_serial(double*, double*, double*, double*, int, int);
int FastUN_serial(double*, double*, double*, double*, int, int);

/** Hard-coded base cases for n = 2, 3 and 5. Called before FastSN to prevent
 *  sine values being recalculated
 */
double* SFactors(int n) {
    double* s = malloc((n + 1) / 2 * sizeof(double));
    int i, j;

    if (!(n & (n - 1))) {
        i = 2;
        j = 1;
    } else if (!(n % 3) && !(n/3 & (n/3 - 1))) {
        *(s++) = 1.0;
        i = 3;
        j = 1;
    } else if (!(n % 5) && !(n/5 & (n/5 - 1))) {
        *(s++) = 1.0;
        *(s++) = sin(M_PI / 5);
        i = 5;
        j = 2;
    } else {
        return(NULL);
    }

    do {
        *(s++) = sin(j++ * M_PI / i);
        if (2 * ++j >= i) {
            i *= 2;
            j = 1;
        }
    } while(i <= n);

    return(s - (n + 1) / 2);
}

/** Discrete Sine Transform (S-matrix) function
 *  x : preallocated output array
 *  y : preallocated input array
 *  w : preallocated temporary storage array
 *  S : precalculated sine values
 *  n : number of array entries being transformed
 *  skip : spacing between array entries being transformed
 */
int FastSN(double* x, double* y, double* w, double* S, int n, int skip) {
    int fails;

    if (n <= threshold) {
        return(FastSN_serial(x, y, w, S, n, skip));
    } else {
        #pragma omp parallel
        #pragma omp single nowait
        fails = FastSN_parallel(x, y, w, S, n, skip);
        return(fails);
    }
}

/** T-matrix function, only called for testing
 *  see FastSN for parameter explanations
 */
int FastTN(double* x, double* y, double* w, double* S, int n, int skip) {
    int fails;

    if (n <= threshold) {
        return(FastTN_serial(x, y, w, S, n, skip));
    } else {
        #pragma omp parallel
        #pragma omp single nowait
        fails = FastTN_parallel(x, y, w, S, n, skip);
        return(fails);
    }
}

/** U-matrix function, only called for testing
 *  see FastSN for parameter explanations
 */
int FastUN(double* x, double* y, double* w, double* S, int n, int skip) {
    int fails;

    if (n <= threshold) {
        return(FastUN_serial(x, y, w, S, n, skip));
    } else {
        #pragma omp parallel
        #pragma omp single nowait
        fails = FastUN_parallel(x, y, w, S, n, skip);
        return(fails);
    }
}

/** parallelized S-matrix function, called in the recursion until n drops below 
 *  threshold. Wrapped by FastSN.
 */
int FastSN_parallel(double* x, double* y, double* w, double* S, int n, int skip) {
    int j, fails1, fails2;

    if (n <= threshold) {
        return(FastSN_serial(x, y, w, S, n, skip));
    }
    if (n % 2 == 0) {

        for (j = 1; j < n / 2; j++) {
            w[(2*j-1)*skip] = y[j*skip] + y[(n-j)*skip];
            w[2*j*skip] = y[j*skip] - y[(n-j)*skip];
        }
        w[(n-1)*skip] = y[n/2*skip];

        #pragma omp task shared(fails1)
        fails1 = FastSN_parallel(x, w, y, S, n / 2, skip * 2);
        #pragma omp task shared(fails2)
        fails2 = FastTN_parallel(x + skip, w + skip, y + skip, S, n / 2, skip * 2);
        #pragma omp taskwait

        fails1 += fails2;

        return(!fails1 ? 0 : -1);
    }
    return(-1);
}

/** parallelized T-matrix function, called in the recursion until n drops below 
 *  threshold. 
 */
int FastTN_parallel(double* x, double* y, double* w, double* S, int n, int skip) {
    int j, fails1, fails2;

    if (n <= threshold) {
        return(FastTN_serial(x, y, w, S, n, skip));
    }
    if (n % 2 == 0) {

        #pragma omp task shared(fails1)
        fails1 = FastTN_parallel(w + skip, y + skip, x + skip, S, n / 2, skip * 2);
        #pragma omp task shared(fails2)
        fails2 = FastUN_parallel(w, y, x, S, n / 2, skip * 2);
        #pragma omp taskwait

        fails1 += fails2;
        
        for (j = 0; j < n/2; j++) {
            x[j*skip] = w[2*j*skip] + w[(2*j+1)*skip];
            x[(n-j-1)*skip] = w[2*j*skip] - w[(2*j+1)*skip];
        }

        return(!fails1 ? 0 : -1);
    }
    return(-1);
}

/** parallelized U-matrix function, called in the recursion until n drops below 
 *  threshold. 
 */
int FastUN_parallel(double* x, double* y, double* w, double* S, int n, int skip) {
    int j, fails1, fails2;
    double k;

    if (n <= threshold) {
        return(FastUN_serial(x, y, w, S, n, skip));
    }
    if (n % 2 == 0) {
        for (j = 1; j < n/2; j++) {
            x[(j-1)*skip] = y[(n+1-2*j)*skip] - y[(n-2*j)*skip];
            x[(j-1+n/2)*skip] = y[(2*j-1)*skip] + y[2*j*skip];
        }
        x[(n/2-1)*skip] = y[0];
        x[(n-1)*skip] = y[(n-1)*skip];

        #pragma omp task shared(fails1)
        fails1 = FastTN_parallel(w, x, y, S, n / 2, skip);
        #pragma omp task shared(fails2)
        fails2 = FastTN_parallel(w + n / 2 * skip, x + n / 2 * skip, y + n / 2 * skip, S, n / 2, skip);
        #pragma omp taskwait

        fails1 += fails2;

        k = 1.0;
        for (j = 0; j < n/2; j++) {
            x[j*skip] = S[n+j] * k * w[j*skip] + S[2*n-j-1] * w[(j+n/2)*skip];
            x[(n-1-j)*skip] = S[2*n-j-1] * k * w[j*skip] - S[n+j] * w[(j+n/2)*skip];
            k = -k;
        }

        return(!fails1 ? 0 : -1);
    }
    return(-1);
}

/** serially computed S-matrix function, called in the recursion once n drops below 
 *  threshold. 
 */
int FastSN_serial(double* x, double* y, double* w, double* S, int n, int skip) {
    int j, fails = 0;
    double temp1, temp2, temp3, temp4;

    if (n == 1) {
        x[skip] = 0.0;
        return(0);
    }
    if (n == 2) {
        x[skip] = y[skip];
        return(0);
    }
    if (n == 3) {
        x[skip]   = S[1] * (y[skip] + y[2*skip]);
        x[2*skip] = S[1] * (y[skip] - y[2*skip]);
        return(0);
    }
    if (n == 5) {
        temp1 = y[skip] + y[4*skip];
        temp2 = y[skip] - y[4*skip];
        temp3 = y[2*skip] + y[3*skip];
        temp4 = y[2*skip] - y[3*skip];

        x[skip]   = S[1] * temp1 + S[2] * temp3;
        x[2*skip] = S[2] * temp2 + S[1] * temp4;
        x[3*skip] = S[2] * temp1 - S[1] * temp3;
        x[4*skip] = S[1] * temp2 - S[2] * temp4;

        return(0);
    }
    if (n % 2 == 0) {

        for (j = 1; j < n / 2; j++) {
            w[(2*j-1)*skip] = y[j*skip] + y[(n-j)*skip];
            w[2*j*skip] = y[j*skip] - y[(n-j)*skip];
        }
        w[(n-1)*skip] = y[n/2*skip];

        fails += FastSN_serial(x, w, y, S, n / 2, skip * 2);
        fails += FastTN_serial(x + skip, w + skip, y + skip, S, n / 2, skip * 2);

        return(!fails ? 0 : -1);
    }
    return(-1);
}

/** serially computed T-matrix function, called in the recursion once n drops below 
 *  threshold. 
 */
int FastTN_serial(double* x, double* y, double* w, double* S, int n, int skip) {
    int j, fails = 0;
    double temp1, temp2, temp3, temp4;

    if (n == 1) {
        x[0] = y[0];
        return(0);
    }
    if (n == 2) {
        temp1 = S[1] * y[0];

        x[0]    = temp1 + y[skip];
        x[skip] = temp1 - y[skip];

        return(0);
    }
    if (n == 3) {
        temp1 = S[2] * y[0] + y[2*skip];
        temp2 = S[1] * y[skip];

        x[0]      = temp1 + temp2;
        x[skip]   = y[0] - y[2*skip];
        x[2*skip] = temp1 - temp2;

        return(0);
    }
    if (n == 5) {
        temp1 = S[3] * y[0] + S[4] * y[2*skip] + y[4*skip];
        temp2 = S[1] * y[skip] + S[2] * y[3*skip];
        temp3 = S[4] * y[0] + S[3] * y[2*skip] - y[4*skip];
        temp4 = S[2] * y[skip] - S[1] * y[3*skip];

        x[0]      = temp1 + temp2;
        x[skip]   = temp3 + temp4;
        x[2*skip] = y[0] - y[2*skip] + y[4*skip];
        x[3*skip] = temp3 - temp4;
        x[4*skip] = temp1 - temp2;
    
        return(0);
    }
    if (n % 2 == 0) {
        fails += FastTN_serial(w + skip, y + skip, x + skip, S, n / 2, skip * 2);
        fails += FastUN_serial(w, y, x, S, n / 2, skip * 2);
        
        for (j = 0; j < n/2; j++) {
            x[j*skip] = w[2*j*skip] + w[(2*j+1)*skip];
            x[(n-j-1)*skip] = w[2*j*skip] - w[(2*j+1)*skip];
        }

        return(!fails ? 0 : -1);
    }
    return(-1);
}

/** serially computed U-matrix function, called in the recursion once n drops below 
 *  threshold. 
 */
int FastUN_serial(double* x, double* y, double* w, double* S, int n, int skip) {
    int j, fails = 0;
    double k, temp1;

    if (n == 1) {
        x[0] = S[1] * y[0];
        return(0);
    }
    if (n == 2) {
        x[0]    = S[2] * y[0] + S[3] * y[skip];
        x[skip] = S[3] * y[0] - S[2] * y[skip];

        return(0);
    }
    if (n == 3) {
        temp1 = S[4] * y[skip];

        x[0]      = S[3] * y[0] + temp1 + S[5] * y[2*skip];
        x[skip]   = S[4] * y[0] + temp1 - S[4] * y[2*skip];
        x[2*skip] = S[5] * y[0] - temp1 + S[3] * y[2*skip];

        return(0);
    }
    if (n == 5) {
        temp1 = S[7] * y[2*skip];

        x[0]      = S[5] * y[0] + S[6] * y[skip] + temp1 + S[8] * y[3*skip] + S[9] * y[4*skip];
        x[skip]   = S[6] * y[0] + S[9] * y[skip] + temp1 - S[5] * y[3*skip] - S[8] * y[4*skip];
        x[2*skip] = S[7] * y[0] + S[7] * y[skip] - temp1 - S[7] * y[3*skip] + S[7] * y[4*skip];
        x[3*skip] = S[8] * y[0] - S[5] * y[skip] - temp1 + S[9] * y[3*skip] - S[6] * y[4*skip];
        x[4*skip] = S[9] * y[0] - S[8] * y[skip] + temp1 - S[6] * y[3*skip] + S[5] * y[4*skip];

        return(0);
    }
    if (n % 2 == 0) {
        for (j = 1; j < n/2; j++) {
            x[(j-1)*skip] = y[(n-2*j)*skip] - y[(n-1-2*j)*skip];
            x[(j-1+n/2)*skip] = y[(2*j-1)*skip] + y[2*j*skip];
        }
        x[(n/2-1)*skip] = y[0];
        x[(n-1)*skip] = y[(n-1)*skip];

        fails += FastTN_serial(w, x, y, S, n / 2, skip);
        fails += FastTN_serial(w + n / 2 * skip, x + n / 2 * skip, y + n / 2 * skip, S, n / 2, skip);

        k = 1.0;
        for (j = 0; j < n/2; j++) {
            x[j*skip] = S[n+j] * k * w[j*skip] + S[2*n-j-1] * w[(j+n/2)*skip];
            x[(n-1-j)*skip] = S[2*n-j-1] * k * w[j*skip] - S[n+j] * w[(j+n/2)*skip];
            k = -k;
        }
        return(!fails ? 0 : -1);
    }
    return(-1);
}
