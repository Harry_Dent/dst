from distutils.core import setup, Extension

extension_dst = Extension(
    "fast_fourier",
    ["dst_wrapper.c", "dst.c", "timer.c"],
    language="c",
    extra_compile_args=["-fopenmp", "-O3"],
    extra_link_args=["-fopenmp"],
)

setup(name="dst", ext_modules=[extension_dst])
