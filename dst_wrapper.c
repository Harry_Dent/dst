#include <Python.h>

double* SFactors(int);
int FastSN(double*, double*, double*, double*, int, int);

double timer(void);

extern int threshold;

/** C code for Python function `dst()`
 *  parameter `args` is a Python numeric list.
 */
static PyObject* dst_wrapper(PyObject* self, PyObject* args) {
    PyObject *x_out, *y_in, *listitem, *ret, *walltime;
    double *y, *x, *w, *S;
    int i, n;

    // parse args, check they are sane
    if (!PyArg_ParseTuple(args, "O", &y_in)) {
        PyErr_SetString(PyExc_TypeError, "Failed to parse input list");
        return(NULL);
    }
    if (!PyList_Check(y_in)) {
        PyErr_SetString(PyExc_TypeError, "Input is not list");
        return(NULL);
    }

    // allocate space for solving the problem
    n = PyList_Size(y_in) + 1;
    x = malloc((n + 1) * sizeof(double));
    y = malloc((n + 1) * sizeof(double));
    w = malloc((n + 1) * sizeof(double));

    S = SFactors(n);

    // copy y into a c array
    for (i = 1; i < n; i++) {
        listitem = PyList_GetItem(y_in, i - 1);
        if (!PyFloat_Check(listitem) && !PyLong_Check(listitem)) {
            PyErr_SetString(PyExc_TypeError, "List contains non-numeric values");
            return(NULL);
        }
        y[i] = PyFloat_AsDouble(listitem);
    }

    // perform transform
    timer();
    if (FastSN(x, y, w, S, n, 1) != 0) {
        PyErr_SetString(PyExc_ValueError, "Vector size not supported - \
            n must be of the form 2^k, 3*2^k or 5*2^k");
        return(NULL);
    }
    walltime = PyFloat_FromDouble(timer());

    x_out = PyList_New(n - 1);

    // copy x to x_out
    for (i = 1; i < n; i++) {
        listitem = PyFloat_FromDouble(x[i]);
        y[i] = PyList_SetItem(x_out, i - 1, listitem);
    }

    // package time and the return matrix together 
    ret = PyTuple_Pack(2, walltime, x_out);

    free(x); free(y); free(w); free(S);

    return(ret);
}

/** get `threshold` in dst.c
 */
static PyObject* get_threshold(PyObject* self, PyObject* args) {
    return PyLong_FromLong(threshold);
}

/** set `threshold` in dst.c
 */
static PyObject* set_threshold(PyObject* self, PyObject* args) {
    if (!PyArg_ParseTuple(args, "l", &threshold)) {
        PyErr_SetString(PyExc_ValueError, "Argument must be integer");
        return(NULL);
    }
    Py_RETURN_NONE;
}

/** instructions for creating Python methods from C functions
 */
static PyMethodDef ff_methods[] = {
    {"dst", dst_wrapper, METH_VARARGS, "performs a fast sine transform"},
    {"get_threshold", get_threshold, METH_VARARGS, ""},
    {"set_threshold", set_threshold, METH_VARARGS, ""},
    { NULL, NULL, 0, NULL }
};

/** struct to define module instance varibles and methods
 */
static struct PyModuleDef ff_module = {
    PyModuleDef_HEAD_INIT,
    "fast_fourier",
    "",
    -1,
    ff_methods
};

/** initialization instruction
 */ 
PyMODINIT_FUNC PyInit_fast_fourier(void) {
    return(PyModule_Create(&ff_module));
}
