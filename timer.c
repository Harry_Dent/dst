#include <stdio.h>

#ifdef __CYGWIN__
	#include <Windows.h>
	
	/** returns the time since the last call of timer(), e.g.
	 *
	 *  timer()
	 *  <..some code here..>
	 *  time_passed = timer()
	 */
	double timer(void) {
		static LARGE_INTEGER s;
		LARGE_INTEGER e, freq;
		double diff;

		// Cygwin needs extra encouragement to use microseconds
		QueryPerformanceFrequency(&freq);
		QueryPerformanceCounter(&e);
		diff = (double) (e.QuadPart - s.QuadPart) / freq.QuadPart;
		QueryPerformanceCounter(&s);

		return(diff);
	}

#else
	#include <sys/time.h>

	/** returns the time since the last call of timer(), e.g.
	 *
	 *  timer()
	 *  <..some code here..>
	 *  time_passed = timer()
	 */
	double timer(void) {
	    static struct timeval s;
	    struct timeval e;
	    double diff;

	    gettimeofday(&e, NULL);
	    diff = e.tv_sec - s.tv_sec + (e.tv_usec - s.tv_usec) / 1000000.0;
	    gettimeofday(&s, NULL);

	    return(diff);
	}
#endif