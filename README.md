# C + Python: fast and easy Discrete Sine Transforms

### What am I looking at?

A parallelized implementation of the Discrete Sine Transform (DST) described in this repo's `abstract.pdf`, wrapped in a Python module.

### Usage

Firstly ensure you have a C compiler that [implements OpenMP](http://bisqwit.iki.fi/story/howto/openmp/#SupportInDifferentCompilers), and set the `CC` environment variable to the name of that compiler. For example, in OSX bash using Homebrew's gcc5:

```
$ export CC=gcc-5
```

Then the Python module is compiled as follows

```
$ python3 setup.py build_ext --inplace
```

The DST function can then be used

```
$ python3
>>> from fast_fourier import dst
>>> n = 8
>>> y = [1.0] * (n - 1)
>>> t, x = dst(y)
>>> print(x)
[5.027339492125848, 0.0, 1.4966057626654887, 0.0, 0.6681786379192989, 0.0, 0.19891236737965823]
```

*This repo is a stripped-down version of my final coursework project at university. Special thanks to Dr Daniel Moore.*